# Javascript Console Mnemo

The browser Javascript console is increalingly powerful.  
[Here](https://oscar6echo.gitlab.io/javascript-console-mnemo/) are some examples for quick access.  

[Gitlab CI/CD](https://docs.gitlab.com/ee/user/project/pages/introduction.html#how-gitlab-ciyml-looks-like-when-the-static-content-is-in-your-repository) is in place and automatically pushes the `master` branch to `pages`.

