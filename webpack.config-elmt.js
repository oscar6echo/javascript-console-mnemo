const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var devMode = process.env.NODE_ENV !== 'production';

optimization: {
    minimizer: !devMode
        ? [
              new UglifyJsPlugin({
                  // Compression specific options
                  uglifyOptions: {
                      // Eliminate comments
                      comments: false,
                      compress: {
                          // remove warnings
                          warnings: false,
                          // Drop console statements
                          drop_console: true,
                      },
                  },
              }),
          ]
        : [];
}
