console.log('CONSOLE.LOG, CONSOLE.INFO, CONSOLE.WARN, CONSOLE.ERROR');

const niceJson = { a: 1, b: 2, c: 3 };

console.log('log', niceJson, new Date());
console.info('info', niceJson, new Date());
console.warn('warn', niceJson, new Date());
console.error('error', niceJson, new Date());

function doSomething(obj, collapsed = true) {
    if (collapsed) {
        console.groupCollapsed('doSomething profile');
    } else {
        console.group('doSomething profile');
    }
    const _data = new Date();
    console.log('evaluating data: ', _data);
    const _fullName = `${obj.firstName} ${obj.lastName}`;
    console.log('fullName: ', _fullName);
    const _id = Math.random(1);
    console.log('id: ', _id);
    console.groupEnd();
}

doSomething(
    { firstName: 'Olivier', lastName: 'Borderies' },
    (collapsed = false)
);
doSomething({ firstName: 'Joe', lastName: 'Doe' }, (collapsed = true));

console.log('CONSOLE.TABLE');

// an array of strings
console.table(['apples', 'oranges', 'bananas']);

// an object whose properties are strings
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}
const me = new Person('John', 'Smith');
console.table(me);

// an array of arrays
const people = [['John', 'Smith'], ['Jane', 'Doe'], ['Emily', 'Jones']];
console.table(people);

// an array of objects
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}
const john = new Person('John', 'Smith');
const jane = new Person('Jane', 'Doe');
const emily = new Person('Emily', 'Jones');
console.table([john, jane, emily]);

// an object whose properties are objects
const family = {};
console.table(family);
family.mother = new Person('Jane', 'Smith');
family.father = new Person('John', 'Smith');
family.daughter = new Person('Emily', 'Smith');
console.log('family');
console.table(family);
const family2 = Object.assign({}, family);
family2.alien = new Date();
console.log('family2');
console.table(family2);

const typeOfConsole = [
    { name: 'log', type: 'standard' },
    { name: 'info', type: 'standard' },
    { name: 'table', type: 'vow' },
];
console.table(typeOfConsole);

const mySocials = {
    facebook: true,
    linkedin: true,
    flickr: false,
    instagram: true,
    VKontakte: false,
};
console.table(mySocials);

// an array of objects, logging only firstName
console.table([john, jane, emily], ['firstName']);

console.log('CONSOLE.COUNT, CONSOLE.TIME, CONSOLE.TIMEEND');

console.time('total');

console.time('init arr');
const arr = new Array(20);
let a = 0;
for (let j = 0; j < 1e7; j++) {
    a += 10;
}
console.timeEnd('init arr');

for (let i = 0; i < arr.length; i++) {
    arr[i] = new Object();
    const _type = i % 2 == 0 ? 'even' : 'odd';
    console.count(_type + ' added');
}

console.log('CONSOLE.ASSERT, CONSOLE.TRACE');

function lessThan(a, b) {
    console.assert(a < b, { message: 'a is not less than b', a: a, b: b });
}
lessThan(6, 5);

console.trace('End');

